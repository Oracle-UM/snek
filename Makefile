CXX   = clang++
FLAGS = -c -W -Wall --std=c++17 -O2

SOURCEDIR = src
BUILDDIR  = build

EXECUTABLE = Snek
SOURCES    = $(wildcard $(SOURCEDIR)/*.cpp)
OBJECTS    = $(patsubst $(SOURCEDIR)/%.cpp, $(BUILDDIR)/%.o, $(SOURCES))

all: dir $(BUILDDIR)/$(EXECUTABLE)

dir:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/$(EXECUTABLE): $(OBJECTS)
	$(CXX) $^ -o $@

$(OBJECTS): $(BUILDDIR)/%.o : $(SOURCEDIR)/%.cpp
	$(CXX) $(FLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
