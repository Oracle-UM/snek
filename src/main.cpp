#include "Game.hpp"

#include <iostream>

int main()
{
    std::ios::sync_with_stdio(false);
    Game::play();
}
