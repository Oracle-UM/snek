#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Map.hpp"

#include <deque>

#include <cstdint>

class Snake
{
   public:
    Snake(Map &);
    void draw() const;
    bool isAlive(const Map &) const;
    void move(Map &);

   private:
    enum class Orientation : std::uint_fast16_t
    {
        North,
        East,
        South,
        West
    };
    using SnakeSegment = std::deque<Map::Coordinates>;
    Orientation s_orientation;
    SnakeSegment s_snake;
    Orientation getOrientationToFurthestWall(const Map &) const;
};

#endif
