#include "Map.hpp"

#include <iostream>
#include <limits>

using namespace std;

Map Map::createMap() { return Map(readDimensions()); }

Map::Map(Map::Coordinates coordinates) :
    m_dimensions(coordinates),
    m_fruit_positions({})
{}

Map::Coordinates Map::readDimensions()
{
    cout << "\nMap width? [" << MinWidth << ", " << MaxWidth << "]" << endl;
    uint_fast16_t width = 0;
    while (!(cin >> width) || width < MinWidth || width > MaxWidth)
    {
        cerr << "Value must be between " << MinWidth << " and " << MaxWidth
             << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    cout << "\nMap height? [" << MinHeight << ", " << MaxHeight << "]" << endl;
    uint16_t height = 0;
    while (!(std::cin >> height) || height < MinHeight || height > MaxHeight)
    {
        cerr << "Value must be between " << MinHeight << " and " << MaxHeight
             << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    return { width, height };
}

void Map::print() const
{
    // north wall
    for (uint_fast16_t num_cols = 0; num_cols < m_dimensions.first; ++num_cols)
    {
        cout << '#';
    }
    cout << '\n';

    // inner map
    for (uint_fast16_t num_rows = 0; num_rows < m_dimensions.second - 2;
         ++num_rows)
    {
        cout << '#';
        for (uint_fast16_t num_cols = 0; num_cols < m_dimensions.first - 2;
             ++num_cols)
        {
            cout << ' ';
        }
        cout << "#\n";
    }

    // south wall
    for (uint_fast16_t num_cols = 0; num_cols < m_dimensions.first; ++num_cols)
    {
        cout << '#';
    }
    cout << '\n';
}
