#ifndef RNG_HPP
#define RNG_HPP

#include <random>

template <class T> class RNG
{
   public:
    RNG() : rng(std::random_device()()) {}
    T operator()(T min, T max)
    {
        std::uniform_int_distribution<T> distr(min, max);
        return distr(rng);
    }

   private:
    std::mt19937 rng;
};

#endif
