#ifndef MAP_HPP
#define MAP_HPP

#include <unordered_set>
#include <utility>

#include <cstdint>

#include <boost/functional/hash.hpp>

class Map
{
   public:
    static Map createMap();
    void print() const;

   private:
    using Coordinates = std::pair<std::uint_fast16_t, std::uint_fast16_t>;
    enum DefaultDimensionValues : std::uint_fast16_t
    {
        MinWidth  = 5,
        MaxWidth  = 100,
        MinHeight = 5,
        MaxHeight = 30
    };
    friend class Snake;
    const Coordinates m_dimensions;
    std::unordered_set<Coordinates, boost::hash<Coordinates>> m_fruit_positions;
    Map(Coordinates);
    static Coordinates readDimensions();
};

#endif
