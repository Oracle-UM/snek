#include "Game.hpp"

#include <iostream>
#include <limits>
#include <string>

using namespace std;

#define INTERVAL_LINE_COUNT 100

#if defined(unix) || defined(__unix__) || defined(__unix)
#    define BOLD_ON "\e[1m"
#    define BOLD_OFF "\e[0m"
#else
#    define BOLD_ON ""
#    define BOLD_OFF ""
#endif

void Game::play()
{
    print();

    if (getMenuOption() == 1)
    {
        Game game = createGame();

        cout << "\nPress [Enter] to play . . ." << endl;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cin.get();

        game.g_map.print();
    }
}

Game::Game(const Map &map, const Snake &snake) : g_map(map), g_snake(snake) {}

Game Game::createGame()
{
    Map map = Map::createMap();
    return { map, Snake(map) };
}

void Game::print()
{
    cout << string(INTERVAL_LINE_COUNT, '\n')
         << "           /^\\/^\\\n"
            "         _|__|  O|\n"
            "\\/     /~     \\_/ \\\n"
            " \\____|__________/  \\\n"
            "        \\_______      \\\n"
            "                `\\     \\                 \\\n"
            "                  |     |                  \\\n"
            "                 /      /                    \\\n"
            "                /     /                       \\\n"
            "              /      /                         \\ \\\n"
            "             /     /                            \\  \\\n"
            "           /     /             _----_            \\   \\\n"
            "          /     /           _-~      ~-_         |   |\n"
            "         (      (        _-~    _--_    ~-_     _/   |\n"
            "          \\      ~-____-~    _-~    ~-_    ~-_-~    /\n"
            "            ~-_           _-~          ~-_       _-~\n"
            "               ~--______-~                ~-___-~\n"
            "\n\t\tWelcome... to "
         << BOLD_ON << "Snek: The Game" << BOLD_OFF "\n"
         << endl;
}

uint_fast16_t Game::getMenuOption()
{
    cout << "Enter an option:\n(1) Play the game\n(2) Exit" << endl;
    int_fast16_t option = 0;
    while (!(cin >> option) || (1 != option && 2 != option))
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return option;
}
