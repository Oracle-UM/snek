#include "Snake.hpp"

#include "RNG.hpp"

#include <algorithm>
#include <array>

using namespace std;

Snake::Snake(Map &map)
{
    RNG<uint_fast16_t> rng;
    s_snake.emplace_front(rng(2, map.m_dimensions.first - 3),
                          rng(2, map.m_dimensions.second - 3));

    s_orientation = getOrientationToFurthestWall(map);
}

bool Snake::isAlive(const Map &map) const
{
    return s_snake.front().first > 0 &&
           s_snake.front().first < map.m_dimensions.first &&
           s_snake.front().second > 0 &&
           s_snake.front().second < map.m_dimensions.second;
}

void Snake::move(Map &map)
{
    switch (s_orientation)
    {
    case Orientation::North:
        s_snake.emplace_front(s_snake.front().first,
                              s_snake.front().second - 1);
        break;
    case Orientation::East:
        s_snake.emplace_front(s_snake.front().first + 1,
                              s_snake.front().second);
        break;
    case Orientation::South:
        s_snake.emplace_front(s_snake.front().first,
                              s_snake.front().second + 1);
        break;
    case Orientation::West:
        s_snake.emplace_front(s_snake.front().first - 1,
                              s_snake.front().second);
        break;
    default:
        break;
    }

    const auto possible_fruit_it = map.m_fruit_positions.find(s_snake.front());
    if (map.m_fruit_positions.cend() == possible_fruit_it)  // fruit not found
    {
        s_snake.pop_back();
    }
    else  // fruit exists
    {
        map.m_fruit_positions.erase(possible_fruit_it);
    }
}
Snake::Orientation Snake::getOrientationToFurthestWall(const Map &map) const
{
    array<uint_fast16_t, 4> distance_to_walls = {
        s_snake.front().second,
        map.m_dimensions.first - s_snake.front().first - 1,
        map.m_dimensions.second - s_snake.front().second - 1,
        s_snake.front().first
    };

    return static_cast<Snake::Orientation>(distance(
        distance_to_walls.cbegin(),
        max_element(distance_to_walls.cbegin(), distance_to_walls.cend())));
}
