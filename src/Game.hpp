#ifndef GAME_HPP
#define GAME_HPP

#include "Map.hpp"
#include "Snake.hpp"

#include <cstdint>

class Game
{
   public:
    static void play();

   private:
    Map g_map;
    Snake g_snake;
    Game(const Map &, const Snake &);
    static Game createGame();
    static void print();
    static std::uint_fast16_t getMenuOption();
};

#endif
